<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainLayoutController extends Controller
{
    public function index()
    {
        return inertia(
            'UI/MainLayoutComponent'
        );
    }
}
